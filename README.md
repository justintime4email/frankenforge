# FrankenForge

## Custom parts and modifications for FlashForge Creator Pro

## TO-DO
- [ ] BIGTREE SKR-2 Marlin Firmware
- [ ] 5V Power Supply
- [ ] Custom Rotary Encoder & Button Interface
- [ ] Raspberry Pi Mount
- [ ] Expansion Board
    - [ ] Case Light
    - [ ] Taiss Probe
- [ ] Magnetic IKEA LEDBERG clips
- [ ] Taiss Capacitive Probe Mount
- [ ] Modified Dual Fan duct
- [ ] Drag Knife Mount
